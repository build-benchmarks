#!/usr/bin/python

import os
import cppcodebase
import random


def CreateLibMakefile(lib_number, classes):
    os.chdir(cppcodebase.lib_name(lib_number))
    handle = file("Makefile.module", "w");
    handle.write('module := %s\n' % (cppcodebase.lib_name(lib_number),))
    handle.write ("lib := $(module)/lib_" + str(lib_number) + ".a\n")
    handle.write ("src := \\\n")
    for i in xrange(classes):
        handle.write('    $(module)/class_' + str(i) + '.cpp \\\n')
    handle.write ("""


SRC += $(src)

obj := $(patsubst %.cpp,%.o,$(src))
dep := $(patsubst %.cpp,%.d,$(src))

include $(dep)

OBJ += $(obj)
LIBS += $(lib)

all: $(lib)

$(lib): $(obj)
	$(ARCHIVE) cr $@ $^
	touch $@

$(obj): INC := -I. -I$(module)
$(dep): INC := -I. -I$(module)

""")
    os.chdir('..')


def CreateFullMakefile(libs):
    handle = file("Makefile", "w")

    handle.write('subdirs = \\\n')
    for i in xrange(libs):
        handle.write('    lib_' + str(i) + '\\\n')
    handle.write("""

COMPILER = g++
CCFLAGS = -g -Wall $(INC)
ARCHIVE = ar
DEPEND = makedepend
.SUFFIXES: .o .cpp

all:

SRC :=
OBJ :=
LIBS :=

include $(patsubst %,%/Makefile.module,$(subdirs))

clean:
	@rm -f $(OBJ) $(LIBS)

%.d: %.cpp
	$(COMPILER) -MM -MG -MT "$*.o $*.d" -MF $@ $(INC) $<

%.o: %.cpp
	$(COMPILER) $(CCFLAGS) $(OUTPUT_OPTION) -c $<

""")

def CreateCodebase(libs, classes, internal_includes, external_includes):
    cppcodebase.SetDir('make_nonrecursive')
    cppcodebase.CreateSetOfLibraries(libs, classes, internal_includes, external_includes, CreateLibMakefile)
    CreateFullMakefile(libs)
    os.chdir('..')
