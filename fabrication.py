#!/usr/bin/python

import os
import cppcodebase
import random


def CreateLibBuildfile(lib, classes):
    pass


def CreateBuildfile(libs, classes):
    handle = file("build.py", "w")

    handle.write("""\
import fabricate

fabricate.setup(runner='strace_runner')

""")
    handle.write('libs = [\n')
    for i in xrange(libs):
        handle.write('    (\'lib_%d\', [\n' % (i,))
        for j in xrange(classes):
            handle.write('        \'class_%d\',\n' % (j,))
        handle.write('        ]),\n')
    handle.write('    ]\n')

    handle.write("""

COMPILER = 'g++'
CCFLAGS = ['-g', '-Wall']
ARCHIVE = 'ar'

for (lib, klasses) in libs:
    for klass in klasses:
        fabricate.run(
            COMPILER, CCFLAGS,
            '-I.',
            '-I%s' % (lib,),
            '-o', '%s/%s.o' % (lib, klass,),
            '-c', '%s/%s.cpp'% (lib, klass,),
            )
    fabricate.run(
        ARCHIVE, 'cr', '%s/%s.a' % (lib, lib),
        ['%s/%s.o' % (lib, klass,) for klass in klasses],
        )

""")

def CreateCodebase(libs, classes, internal_includes, external_includes):
    cppcodebase.SetDir('fabricate')
    cppcodebase.CreateSetOfLibraries(libs, classes, internal_includes, external_includes, CreateLibBuildfile)
    CreateBuildfile(libs, classes)
    os.chdir('..')

