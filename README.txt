This project includes some scripts for benchmarking build tools
against one another.  The original version was downloaded from here:

    http://www.gamesfromwithin.com/wp-content/uploads/bin/generate_libs.zip

and described in these blog posts:

    http://gamesfromwithin.com/?p=42
    http://gamesfromwithin.com/?p=44

Noel Llopis, the original author, stated in an email to me that
"You're welcome to extend the scripts, patch them, and distribute them
in any way you want."

To get started, look at run-benchmarks.sh and adapt it to your needs
and local environment.
