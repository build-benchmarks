#! /bin/sh

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Don't forget to turn the CPU up to 100%!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PYTHONPATH=$HOME/self/proj/fabricate/trunk-read-only
export PYTHONPATH

rm -rf libs && ./generate_libs.py libs 50 100 15 5

(cd libs/make/ && time make)
(cd libs/make/ && time make)

(cd libs/make_nonrecursive/ && time make)
(cd libs/make_nonrecursive/ && time make)

(cd libs/scons && time scons)
(cd libs/scons && time scons)

(cd libs/fabricate && time python ./build.py)
(cd libs/fabricate && time python ./build.py)

